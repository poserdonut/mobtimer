import { Component, OnInit } from '@angular/core';
import { MatIconModule } from '@angular/material';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  hej(): void {
    console.log('Settings?');
  }

}
