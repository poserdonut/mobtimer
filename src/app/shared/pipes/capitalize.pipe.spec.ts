import { CapitalizePipe } from './capitalize.pipe';

describe('CapitalizePipe', () => {
  it('create an instance', () => {
    const pipe = new CapitalizePipe();
    expect(pipe).toBeTruthy();
  });

  it('capitalize word', () => {
    const pipe = new CapitalizePipe();
    let result = pipe.transform("jens");
    expect(result).toEqual('Jens');
  });

  it('capitalize word should handle empty strings', () => {
    const pipe = new CapitalizePipe();
    let result = pipe.transform('');
    expect(result).toEqual('');
  });
});