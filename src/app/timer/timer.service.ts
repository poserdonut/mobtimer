import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';

@Injectable()
export class TimerService {

  private timeInSeconds: number = 15 * 60;
  private secondsRemaining: number;

  public isRunning: boolean;
  public hasFinished: boolean;
  public hasStarted: boolean;

  public displayTimeSubject = new BehaviorSubject(null);
  public progressSubject = new BehaviorSubject(0);
  public hasFinishedSubject = new BehaviorSubject(false);

  constructor() {
      this.secondsRemaining = this.timeInSeconds;
      this.displayTimeSubject.next(this.toDisplayFormat());
  }

   public startTimer(): void {
     this.isRunning = true;
     this.hasStarted = true;
     this.hasFinished = false;
     this.timerTick();
   }

   private timerTick(): void {
      setTimeout(() => {
          if (!this.isRunning) { return; }
          this.secondsRemaining--;
          this.updateProgress();
          this.updateDisplayText();
          if (this.secondsRemaining > 0) {
              this.timerTick();
          } else {
              this.hasFinished = true;
              this.hasFinishedSubject.next(true);
              this.isRunning = false;
          }
      }, 1000);
   }

   public setTime(minutes: number) {
     this.timeInSeconds = this.minutesToSeconds(minutes);
     this.resetTimer();
     this.updateDisplayText();
     this.updateProgress();
   }

   public pauseTimer(): void {
     this.isRunning = false;
   }

   public resumeTimer(): void {
     this.startTimer();
   }

    public resetTimer(): void {
     this.secondsRemaining = this.timeInSeconds;
     this.isRunning = false;
     this.hasFinished = false;
     this.hasStarted = false;
     this.updateProgress();
     this.updateDisplayText();
   }

   private minutesToSeconds(minutes: number) {
     return minutes * 60;
   }

   private updateProgress(): void {
      this.progressSubject.next(this.progress());
   }

   public progress(): number {
     return (1 - (this.secondsRemaining / this.timeInSeconds)) * 100;
   }

   private updateDisplayText(): void {
     this.displayTimeSubject.next(this.toDisplayFormat());
   }

  public toDisplayFormat(): string {
      const hours   = Math.floor(this.secondsRemaining / 3600);
      const minutes = Math.floor((this.secondsRemaining - (hours * 3600)) / 60);
      const seconds = this.secondsRemaining - (hours * 3600) - (minutes * 60);
      let minutesString = '';
      let secondsString = '';
      minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
      secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
      return minutesString + ':' + secondsString;
  }

}
