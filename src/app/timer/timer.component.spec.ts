import { TimerService } from './timer.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimerComponent } from './timer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule, MatSliderModule, MatSnackBarModule } from '@angular/material';
import { MobstersService } from '../mobsters/mobsters.service';
import { NotificationService } from '../shared/services/notification.service';

describe('TimerComponent', () => {
  let component: TimerComponent;
  let fixture: ComponentFixture<TimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimerComponent ],
           imports: [        
        BrowserAnimationsModule,
        MatGridListModule, 
        MatSliderModule, 
        MatSnackBarModule
        ],
      providers: [TimerService, MobstersService, NotificationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
