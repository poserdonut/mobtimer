import { TimerService } from './timer.service';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MobstersService } from '../mobsters/mobsters.service';
import { NotificationService } from '../shared/services/notification.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  public mobsters: Array<string> = [];
  public displayTime: string;

  constructor(private timerService: TimerService, private mobsterService: MobstersService, private notificationService: NotificationService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.timerService.displayTimeSubject.subscribe((displayTime: string) => this.displayTime = displayTime);

    this.timerService.hasFinishedSubject.subscribe((hasFinished: boolean) => {
      if(hasFinished) {
        this.rotateAndReset()
      }
    });
  }

  setTime(minutes: number) {
    this.timerService.setTime(minutes);
  }

  pauseTimer(): void {
    this.timerSnackBar("Pausing")
    this.timerService.pauseTimer();
  }

  resumeTimer(): void {
    this.timerSnackBar("Starting")
    this.timerService.resumeTimer();
  }

  resetTimer(): void {
    this.timerSnackBar("Resetting")
    this.timerService.resetTimer();
  }

  isTimerRunning(): boolean {
    return this.timerService.isRunning;
  }

  isTimerStarted(): boolean {
    return this.timerService.hasStarted;
  }

  isResumeDisabled(): boolean {
    return (this.timerService.hasStarted && this.timerService.isRunning) || this.timerService.hasFinished || this.mobsterService.getMobsters().length <= 0;
  }

  isResetDisabled(): boolean {
    return !this.timerService.hasStarted;
  }

  getStartResumeButtonText(): string {
    if(this.timerService.hasStarted) {
      return "RESUME"
    }

    return "START";
  }

  private timerSnackBar(action: string): void {
    this.snackBar.open(`${action} timer for ${this.mobsterService.getCurrentMobster()}`, undefined, 
    { duration: 500, horizontalPosition: "center" });
  }

  private rotateAndReset(): void {
    this.timerService.resetTimer();
    this.mobsterService.rotate();
    this.snackBar.open(`Time to switch, ${this.mobsterService.getCurrentMobster()} is up`, undefined, { duration: 1000})

    console.log("Notification is supported " + this.notificationService.isSupported());
    if(this.notificationService.isSupported()) {
      this.notificationService.create("Time to switch mobster", {body: "Next up is " + this.mobsterService.getCurrentMobster()}).subscribe(x => {
        console.log(x);
      });
    }
  }


}
