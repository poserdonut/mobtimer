import { Component, Inject } from '@angular/core';
import { MobstersService } from './mobsters.service';
import { TimerService } from '../timer/timer.service';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-mobsters',
  templateUrl: './mobsters.component.html',
  styleUrls: ['./mobsters.component.css']
})
export class MobstersComponent {

  constructor(private mobsterService: MobstersService, private timerService: TimerService, public snackBar: MatSnackBar, public dialog: MatDialog) { }

  mobsters(): Array<string> {
    return this.mobsterService.getMobsters();
  }

  rotate() {
    this.mobsterService.rotate();
    this.timerService.resetTimer();
  }

  add(mobster: string): void {
    this.mobsterService.addMobster(mobster);
  }

  remove(mobster: string): void {
    this.mobsterService.removeMobster(mobster);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MobstersAddMobsterDialog, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null && result.length > 0) {
        this.mobsterService.addMobster(result);
      }
    });
  }
}

@Component({
  selector: 'mobsters-add-mobster-dialog',
  templateUrl: 'mobsters-add-mobster-dialog.html',
})
export class MobstersAddMobsterDialog {

  constructor(
    public dialogRef: MatDialogRef<MobstersAddMobsterDialog>,
    @Inject(MAT_DIALOG_DATA) public data: string) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
