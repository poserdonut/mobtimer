import { TestBed, inject } from '@angular/core/testing';

import { MobstersService } from './mobsters.service';

describe('MobstersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MobstersService]
    });
  });

  it('should be created', inject([MobstersService], (service: MobstersService) => {
    expect(service).toBeTruthy();
  }));
});
