import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobstersComponent } from './mobsters.component';
import { MatListModule, MatChipsModule, MatIconModule, MatSnackBar, MatDialogModule } from '@angular/material';
import { MobstersService } from './mobsters.service';
import { TimerService } from '../timer/timer.service';
import { OVERLAY_PROVIDERS } from '@angular/cdk/overlay';
import { CapitalizePipe } from '../shared/pipes/capitalize.pipe';

describe('MobstersComponent', () => {
  let component: MobstersComponent;
  let fixture: ComponentFixture<MobstersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatListModule,
        MatChipsModule,
        MatIconModule,
        MatDialogModule
      ],
      declarations: [ MobstersComponent, CapitalizePipe ],
      providers: [MobstersService, TimerService, MatSnackBar, OVERLAY_PROVIDERS]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobstersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
