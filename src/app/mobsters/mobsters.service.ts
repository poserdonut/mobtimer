import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class MobstersService {
  private mobsters: Array<string> = [];
  private readonly storageKey = 'mobsters';

  constructor() {
    this.init();
  }

  init() {
    this.mobsters = this.getMobstersFromStorage();
  }

  rotate(): void {
    const lastMobster = this.mobsters.shift();
    this.mobsters.push(lastMobster)
    this.updateStorage();
  }

  addMobster(name: string) {
    this.mobsters.push(name);
    this.updateStorage();
  }

  removeMobster(name: string) {
    const index = this.mobsters.indexOf(name, 0);
    if (index > -1) {
      this.mobsters.splice(index, 1);
    }
    this.updateStorage();
  }

  getMobsters(): Array<string> {
    return this.mobsters;
  }

  getCurrentMobster(): string {
    return this.mobsters[0];
  }

  clearMobsters(): void {
    this.mobsters = [];
    this.updateStorage();
  }

  private updateStorage(): void {
      localStorage.setItem(this.storageKey, JSON.stringify(this.mobsters));
  }

  private getMobstersFromStorage(): Array<string> {
    if (!localStorage.getItem(this.storageKey)) {
      return [];
    }

    const storedMobsters = JSON.parse(localStorage.getItem(this.storageKey));
    return storedMobsters;
  }

}
