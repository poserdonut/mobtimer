import { TimerService } from '../timer/timer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.css']
})
export class ProgressbarComponent implements OnInit {
  private _timerService: TimerService;
  public progress = 0;

  constructor(timerService: TimerService) {
    this._timerService = timerService;
  }

  ngOnInit() {
    this._timerService.progressSubject.subscribe((progress: number)  => this.progress = progress);
  }

}
