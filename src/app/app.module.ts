import { TimerService } from './timer/timer.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { MatIconModule, MatToolbarModule, MatFormFieldModule, MatSliderModule,
         MatProgressBarModule, MatInputModule,
         MatButtonModule,
         MatGridListModule,
         MatListModule,
         MatSnackBarModule,
         MatChipsModule,
         MatDialogModule
        } from '@angular/material';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { TimerComponent } from './timer/timer.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { MobstersComponent, MobstersAddMobsterDialog } from './mobsters/mobsters.component';
import { MobstersService } from './mobsters/mobsters.service';
import { NotificationService } from './shared/services/notification.service';
import { CapitalizePipe } from './shared/pipes/capitalize.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    TimerComponent,
    ProgressbarComponent,
    MobstersComponent,
    CapitalizePipe,
    MobstersAddMobsterDialog
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatSliderModule,
    MatInputModule,
    MatButtonModule,
    MatProgressBarModule,
    MatGridListModule,
    MatListModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatChipsModule,
    MatDialogModule
  ],
  providers: [TimerService, MobstersService, NotificationService],
  entryComponents: [MobstersAddMobsterDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
