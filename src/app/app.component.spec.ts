import { TimerService } from './timer/timer.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NavigationComponent } from './navigation/navigation.component';
import { TimerComponent } from './timer/timer.component';
import { TestBed, async } from '@angular/core/testing';


import { AppComponent } from './app.component';
import { MobstersComponent } from './mobsters/mobsters.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { MatGridListModule, MatSliderModule, MatIconModule, MatToolbarModule, MatChipsModule, MatProgressBarModule, MatSnackBar, MatDialogModule } from '@angular/material';
import { NotificationService } from './shared/services/notification.service';
import { MobstersService } from './mobsters/mobsters.service';
import { OVERLAY_PROVIDERS } from '@angular/cdk/overlay';
import { CapitalizePipe } from './shared/pipes/capitalize.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TimerComponent,
        NavigationComponent,
        MobstersComponent,
        ProgressbarComponent,
        CapitalizePipe
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        BrowserAnimationsModule,
        MatGridListModule,
        MatSliderModule,
        MatIconModule,
        MatToolbarModule,
        MatChipsModule,
        MatProgressBarModule,
        MatDialogModule
      ],
      providers: [TimerService, NotificationService, MobstersService, MatSnackBar, OVERLAY_PROVIDERS]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
